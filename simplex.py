#!/usr/bin/env python3

import numpy as np

class C:

    """ Cette classe code une contrainte lineaire de la forme Ax <= b """

    def __init__(self, A, b):
        self.A = np.array(A, dtype="float64")
        self.b = np.array(b, dtype="float64")

        m = len(A)
        n = len(self.b)
        assert m == n

    def __and__(self, p2):

        """ Renvoie la conjonction de deux contraintes (avec &) """

        # On vérifie que les contraintes concernent le même x
        _, n1 = self.A.shape
        _, n2 = p2.A.shape
        assert n1 == n2

        A = np.concatenate((self.A, p2.A))
        b = np.append(self.b, p2.b)
        return C(A, b)

    def __repr__(self):
        return "A={} b={}".format(repr(self.A), repr(self.b))



def has_sol(constr):

    """ Resout le probleme du vide de { x | Ax <= b} par une méthode
    d’enveloppe convexe """ 

    # TODO : calculs rapides en virgule fixée

    # FIXME : les calculs sur les flottants entraînent des divisions par 0 !!!

    A = constr.A
    b = constr.b

    m,n = np.shape(A)

    # on définit la borne B
    C = max(np.max(A), np.max(b))
    B = np.math.factorial(m)*C**m

    # on altère le programme linéaire initial
    b = b + 1/(2*n*B)*np.ones(m, dtype="float64")

    # on construit le simplexe initial
    v = np.zeros((n+1,n))
    for i in range(n): v[0][i] = - 2**(n-1-i) * B
    v[1] = np.copy(v[0]); v[1][0] = -v[1][0]
    for i in range(2,n+1):
        for j in range(i): v[i][j] = 0
        if i < n: v[i][i] = 3 * B * 2**(n-i)
        for j in range(i+1, n):
            v[i][j] = - 2**(n-j-1) * B

    t = 3 + np.ceil(3*np.log2(n+1))

    # TODO : établir le volume de S' afin de possiblement réduire le
    # nombre d’itérations plus finement
    cou = 0
    mcou = 2*(n+1)**2*np.log(2**((n*(n+1))//2)*C**n*2*n*n*C*B)

    while cou <= mcou:
        cou += 1

        # c est l’isobarycentre du simplexe
        c = np.mean(v, axis=0)

        u = np.dot(A, c)

        # le centre est une solution
        if np.all(u <= b): 
            return True

        # si ça n’est pas le cas, on recalcule le simplexe
        i = min(j for j in range(m) if u[j] > b[j])
        a = A[i]
        l = min(range(n+1), key=lambda j: np.dot(a, v[j]))
        vl = np.copy(v[l])
        d = np.dot(a, vl)

        if np.all(d >= b):
            return False

        y = 1/(n*n*np.dot(a, c-vl))
        z = 2**t
        x = 2**(-t)
        for i in range(n+1):
            delt = 1/(1 - np.dot(a, c-v[i])*y)
            delt = np.ceil(z * delt) * x
            v[i] = (1-delt)*vl + delt*v[i]

    return False


class Solver:

    """ Resout un programme lineaire sous forme standard, ie
    max(c.x | A.x = b /\ x >= 0) """

    def __init__(self, A, b, c):
        self.A = np.array(A, dtype="float64")
        self.b = np.array(b, dtype="float64")
        self.c = np.array(c, dtype="float64")

    def load(file):
        """ Charge un programme lineaire depuis un fichier """
        # TODO on ne parle ici que de simples programmes linéaires
        # pour le cas général (avec disjonctions), on mettre le programme sous forme DNF

        # TODO inférer la précision depuis le fichier

        # format de fichier possible :
        # c
        # couples matrice/vecteurs séparés par des lignes avec &
        fd = open(file, "r")
        c = np.array([float(i) for i in fd.readline().strip().split()], dtype="float64")
        b = np.array([float(i) for i in fd.readline().strip().split()], dtype="float64")
        u = []
        for line in fd:
            u.append([float(i) for i in line.strip().split()])
        fd.close()
        A = np.array(u, dtype="float64")
        m,n = A.shape
        assert m == len(b) and n == len(c)

        return Solver(A, b, c)

    def save(self, file):
        """ Sauvegarde un programme lineaire standard dans un fichier """

        fd = open(file, "w")
        fd.write(" ".join(str(x) for x in self.c)); fd.write('\n')
        fd.write(" ".join(str(x) for x in self.b)); fd.write('\n')
        for v in self.A:
            fd.write(" ".join(str(x) for x in v)); fd.write('\n')
        fd.close()


    def solve(self):

        """ Resout le problème de programmation lineaire par reduction au probleme
        d'existence """

        m, n = self.A.shape
        Cm = max(np.max(self.A), np.max(self.b), np.max(self.c))
        B = np.math.factorial(m)*Cm**m

        # Le problème de base : Ax = b /\ x >= 0
        bp = C(self.A, self.b) & C(-self.A, -self.b) & C(-np.identity(n), np.zeros(n))

        # On regarde si P admet une solution
        if not has_sol(bp):
            return None

        # P est-il majoré ?
        if has_sol(bp & C([-self.c], [-(n*Cm*B+1)])):
            return "Unbounded"

        # Encadrement de la solution optimale par dichotomie
        b1 = -int(n*B*Cm**3)
        b2 = -b1
        while b1+1 < b2:
            m = (b1 + b2) >> 1
            if has_sol(bp & C([-self.c], [-m/B])):
                b1 = m
            else:
                b2 = m
        p = b1

        # Construction d’une base de solution optimale
        bp = bp & C([-self.c], [-p/(B*B)]) & C([self.c], [(p+1)/(B*B)])
        S = set()
        for j in range(n):
            Cj = C([[int(i==j) for i in range(n)]], [0])
            if has_sol(bp & Cj):
                S.add(j)
                bp = bp & Cj

        A = self.A[:,list(S)]

        # le support étant déterminé, il ne reste plus qu’à résoudre
        # le système Ax = b /\ (x_i = 0 pour i dans S) (qui est de cramer
        # d'après le cours)
        x1 = np.linalg.solve(A, self.b)
        d = dict(zip(S,x1))
        x = np.array([d.get(i,0) for i in range(n)])
        return (x, np.dot(self.c, x))



def randProb(m, n):
    """ Genere un programme lineaire aleatoire """
    # TODO : trouver une distribution "intéressante"
    A = np.random.sample(m*n).reshape(m,n)
    b = np.random.sample(m)
    c = np.random.sample(n)
    return Solver(A, b, c)


if __name__ == '__main__':
    # Exemple tout simple pour comparer avec un autre solver de programmes
    # linéaires
    for i in range(20):
        p = randProb(1,3)
        p.save("p{}.txt".format(i))
        try:
            print('{}:'.format(i), p.solve())
        except ValueError:
            print('{}: failed :('.format(i))
        

# Exemple:
# A = np.array([[1,1],[0,2]])
# b = np.array([2,4])
# c = np.array([1,1])
# p = Solver(A, b, c)
# p.save("pb.txt")
# print(p.solve())
